provider "aws" {
  region     = "eu-west-3"
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "example_subnet" {
  vpc_id     = aws_vpc.example.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-3a"
  map_public_ip_on_launch = true  # Permet aux instances d'obtenir une IP publique
}

resource "aws_security_group" "example" {
  name        = "my-example-security-group"
  description = "Example security group"
  vpc_id      = aws_vpc.example.id  # Assurez-vous que le groupe de sécurité appartient au même VPC que le sous-réseau

  # Autorise le trafic SSH entrant
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Autorise tout le trafic sortant
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id
}

resource "aws_route_table" "example_public" {
  vpc_id = aws_vpc.example.id
}

resource "aws_route" "example_public" {
  route_table_id         = aws_route_table.example_public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.example.id
}

resource "aws_instance" "example" {
  ami           = "ami-0f014d1f920a73971"
  instance_type = "t2.micro"
  key_name      = "keypair2"
  subnet_id     = aws_subnet.example_subnet.id
  vpc_security_group_ids = [aws_security_group.example.id]  # Utilisez vpc_security_group_ids au lieu de security_groups

  tags = {
    Name = "vault-test-3"
  }
}

resource "aws_route_table_association" "example" {
  subnet_id      = aws_subnet.example_subnet.id
  route_table_id = aws_route_table.example_public.id
}
