listener "tcp" {
  address       = "0.0.0.0:8200"
  cluster_address = "0.0.0.0:8201"
  tls_disable    = 1  
}

storage "mysql" {
  address = "15.237.249.64:3306"
  username = "vaultuser"
  password = "vaultpassword"
  database = "vault"
}

api_addr = "http://0.0.0.0:8200"

ui = true

